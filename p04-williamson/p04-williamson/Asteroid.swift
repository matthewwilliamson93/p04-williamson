//
//  Asteroid.swift
//  p04-williamson
//
//  Created by Matthew Williamson on 2/13/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

import SpriteKit

class Asteroid : SKNode {

    var node: SKSpriteNode
    let nodeSize : Int
    
    init(s: Int = 3, pos: CGPoint) {
        nodeSize = s
        node = SKSpriteNode(imageNamed: "asteroid" + String(arc4random_uniform(5) + 1))
        switch s {
        case 3:
            node.setScale(0.7)
        case 2:
            node.setScale(0.5)
        case 1:
            node.setScale(0.3)
        default:
            fatalError("Passes inappropriate pass to the constructor, size should be 1, 2, or 3")
        }
        super.init()
        
        name = "asteroid"
        physicsBody = SKPhysicsBody(circleOfRadius: CGFloat(node.size.width/2))
        physicsBody?.categoryBitMask = asteroidCategory
        physicsBody?.contactTestBitMask = shipCategory | bulletCategory
        physicsBody?.collisionBitMask = 0
        
        position = pos
        let speedX = (Int(arc4random_uniform(25)) + 25) * (Int(arc4random_uniform(2)) * 2 - 1)
        let speedY = (Int(arc4random_uniform(25)) + 25) * (Int(arc4random_uniform(2)) * 2 - 1)
        
        physicsBody?.velocity = CGVectorMake(CGFloat(speedX), CGFloat(speedY))
        physicsBody?.linearDamping = 0
        
        let theta = Float(arc4random_uniform(UInt32.max))/Float(UInt32.max) * Float(M_PI) * 2.0
        physicsBody?.angularVelocity = CGFloat(theta)
        physicsBody?.angularDamping = 0

        addChild(node)
    }
    
    func update(currentTime: CFTimeInterval) {
        let xPadding = node.size.width/4
        let yPadding = node.size.height/4
        
        if (position.x < -xPadding) {
            position.x = scene!.size.width + xPadding
        } else if (position.x > (scene!.size.width + xPadding)) {
            position.x = -xPadding
        }
        
        if (position.y < -yPadding) {
            position.y = scene!.size.height + yPadding
        } else if(position.y > (scene!.size.height + yPadding)) {
            position.y = -yPadding
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}