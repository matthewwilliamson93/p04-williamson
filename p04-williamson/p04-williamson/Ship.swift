//
//  Ship.swift
//  p04-williamson
//
//  Created by Matthew Williamson on 2/15/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

import SpriteKit

class Ship : SKNode {
    
    var node: SKSpriteNode
    var sceneWidth: CGFloat
    var sceneHeight: CGFloat
    
    init(pos: CGPoint) {
        node = SKSpriteNode(imageNamed: "Spaceship")
        node.setScale(0.15)
        sceneWidth = pos.x * 2
        sceneHeight = pos.y * 2
        super.init()
        
        name = "ship"
        physicsBody = SKPhysicsBody(circleOfRadius: CGFloat(node.size.width/2))
        physicsBody?.categoryBitMask = shipCategory
        physicsBody?.contactTestBitMask = asteroidCategory
        physicsBody?.collisionBitMask = 0
        
        position = pos
        physicsBody?.velocity = CGVectorMake(0, 0)
        physicsBody?.linearDamping = 0
        physicsBody?.angularVelocity = 0
        physicsBody?.angularDamping = 0
        
        addChild(node)
    }
    
    func update(currentTime: CFTimeInterval) {
        let xPadding = node.size.width/4
        let yPadding = node.size.height/4
        
        if (position.x < -xPadding) {
            position.x = sceneWidth + xPadding
        } else if (position.x > (sceneWidth + xPadding)) {
            position.x = -xPadding
        }
        
        if (position.y < -yPadding) {
            position.y = sceneHeight + yPadding
        } else if(position.y > (sceneHeight + yPadding)) {
            position.y = -yPadding
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}